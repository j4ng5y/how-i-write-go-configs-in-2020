package main

import (
	"log"

	"github.com/spf13/viper"
	"gitlab.com/j4ng5y/how-i-write-go-configs-in-2020/server"
)

func main() {
	vCfg := viper.New()
	vCfg.BindEnv("server.ip_address", "SCENARIO1_IPADDR")
	vCfg.SetDefault("server.ip_address", "0.0.0.0")
	vCfg.BindEnv("server.port", "SCENARIO1_PORT")
	vCfg.SetDefault("server.port", 8080)
	vCfg.BindEnv("server.timeout.write", "SCENARIO1_WRITE_TIMEOUT")
	vCfg.SetDefault("server.timeout.write", 30)
	vCfg.BindEnv("server.timeout.read", "SCENARIO1_READ_TIMEOUT")
	vCfg.SetDefault("server.timeout.read", 30)
	vCfg.BindEnv("server.timeout.idle", "SCENARIO1_IDLE_TIMEOUT")
	vCfg.SetDefault("server.timeout.idle", 45)
	vCfg.BindEnv("server.timeout.server", "SCENARIO1_SERVER_TIMEOUT")
	vCfg.SetDefault("server.timeout.server", 60)

	S, err := server.New(vCfg)
	if err != nil {
		log.Fatal(err)
	}

	S.Run()
}
