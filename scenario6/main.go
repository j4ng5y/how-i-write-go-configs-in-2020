package main

import (
	"log"

	"github.com/spf13/viper"
	"gitlab.com/j4ng5y/how-i-write-go-configs-in-2020/server"
)

func main() {
	vCfg := viper.New()
	vCfg.Set("server.ip_address", "0.0.0.0")
	vCfg.Set("server.port", 8080)
	vCfg.Set("server.timeout.write", 30)
	vCfg.Set("server.timeout.read", 30)
	vCfg.Set("server.timeout.idle", 45)
	vCfg.Set("server.timeout.server", 60)

	S, err := server.New(vCfg)
	if err != nil {
		log.Fatal(err)
	}

	S.Run()
}
