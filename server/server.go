package server

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/spf13/viper"
	handler "gitlab.com/j4ng5y/how-i-write-go-configs-in-2020/server/handlers"
)

type Server struct {
	HTTPServer *http.Server
	Config     *viper.Viper
}

func New(cfg *viper.Viper) (*Server, error) {
	S := &Server{}
	S.Config = cfg
	Router := new(http.ServeMux)
	Router.HandleFunc("/", handler.RootHandler)

	S.HTTPServer = &http.Server{
		Addr:         fmt.Sprintf("%s:%d", S.Config.GetString("server.ip_address"), S.Config.GetInt("server.port")),
		Handler:      Router,
		WriteTimeout: S.Config.GetDuration("server.timeout.write") * time.Second,
		ReadTimeout:  S.Config.GetDuration("server.timeout.read") * time.Second,
		IdleTimeout:  S.Config.GetDuration("server.timeout.idle") * time.Second,
	}

	return S, nil
}

func (S *Server) Run() {
	runChan := make(chan os.Signal, 1)
	ctx, cancel := context.WithTimeout(context.Background(), S.Config.GetDuration("server.timeout.server")*time.Second)
	defer cancel()

	signal.Notify(runChan, os.Interrupt, syscall.SIGTSTP)

	log.Printf("running server at %s\n", S.HTTPServer.Addr)
	go func() {
		if err := S.HTTPServer.ListenAndServe(); err != nil {
			if err == http.ErrServerClosed {
				// graceful behavior
			} else {
				log.Fatal(err)
			}
		}
	}()

	killCmd := <-runChan

	log.Printf("shutting server down due to %s", killCmd.String)
	if err := S.HTTPServer.Shutdown(ctx); err != nil {
		log.Fatal(err)
	}
}
