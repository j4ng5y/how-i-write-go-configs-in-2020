package main

import (
	"log"

	"github.com/spf13/viper"
	"gitlab.com/j4ng5y/how-i-write-go-configs-in-2020/server"
)

func main() {
	vCfg := viper.New()
	vCfg.SetConfigName("config")
	vCfg.AddConfigPath(".")
	if err := vCfg.ReadInConfig(); err != nil {
		log.Fatal(err)
	}

	S, err := server.New(vCfg)
	if err != nil {
		log.Fatal(err)
	}

	S.Run()
}
